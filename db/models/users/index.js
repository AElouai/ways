const db = require('../../index');


const UserSchema = new db.Schema({
  email: {type: String,},
  /**
   * When the user was created
   */
  creationDate: { type: Date, default: Date.now, select: false },
  /**
   * When the user was Updated the last time to prioritize witch version to save when sync
   */
  updateDate: { type: Date, default: Date.now, select: false },
  /**
   * firstName/lastName is a bit outdated
   * i probably should replace them with full name instead
   */
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  /**
   * Phone number number
   */
  phone: String,

});


module.exports = db.model('User', UserSchema);
