let mongoose = require('mongoose');

//it not supported by heroku
mongoose.Promise = global.Promise;

var options = {
    // server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
    // replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS : 30000 } },
    //Todo : hi don't got to production with this password xD
    // for more visite
    //https://docs.mongodb.com/manual/tutorial/enable-authentication/
    /*user: 'ali',
    pass: 'ali'*/
};


let url = process.env.MONGODB_URI || 'mongodb://localhost/waystocap';

mongoose.connect(url , options, function () {
    console.log('mongodb connected',url);
});

module.exports = mongoose;
