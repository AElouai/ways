// parse the body to json
import bodyParser from "body-parser";
import compression from "compression";
// used server
import express from "express";
// for managing logging
import morgan from "morgan";

//api endpoints
import Api from "./api";

const port = parseInt(process.env.PORT, 10) || 42001;
const dev = process.env.NODE_ENV !== "production";

const server = express();

const allowCrossDomain = function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "*");

  next();
};

server.use(compression());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));
server.use(morgan("dev"));
server.use(express.static(__dirname + "/../dist"));
server.use(allowCrossDomain);

server.use("/api", Api);

server.get('*', function(req, res) {
  res.sendfile('./dist/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});
server.listen(port, err => {
    if (err) {
      console.log(`> NoReady : `, err);
      throw err;
    }
    console.log(`> Ready on Port :${port}`);
});
