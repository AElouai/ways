import get from './get';
import post from './post';
import put from './put';

import express from 'express';

const router = express.Router();

router.use(post);
router.use(get);
router.use(put);


export default router;
