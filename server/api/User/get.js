import express from 'express';
const User = require('../../../db/models/users');

const router = express.Router();

router.get('/', function (req, res, next) {

  User.find({})
    .exec(function (err, users) {
      if (err) { return next(err) }
      res.json(users);
    })
});


router.get('/:id', function (req, res, next) {

  User.findOne({ _id : req.params.id })
    .exec(function (err, user) {
      if (err) { return next(err) }
      res.json(user);
    })
});


export default router;
