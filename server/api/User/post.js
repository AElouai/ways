import express from "express";
const User = require("../../../db/models/users");

const router = express.Router();

router.post("/", function(req, res, next) {
  const user = new User({
    email: req.body.email,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    phone: req.body.phone,
  });

  user.save(function(err) {
    if (err) {
      console.log("err", err);
      res.status(401).send(err);
    } else {
      res.status(201).json({
          _id: user._id,
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
        phone: user.phone
      });
    }
  });
});

router.post("/sync", function(req, res, next) {
  req.body.map(el =>{
    const user = new User({
      email: el.email,
      firstName: el.firstName,
      lastName: el.lastName,
      phone: el.phone,
    });
    user.save();
  });
  res.status(201).json("done");
});

export default router;
