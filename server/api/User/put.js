import express from "express";
const User = require("../../../db/models/users");

const router = express.Router();

router.put("/sync", function(req, res, next) {
  req.body.new.map(el =>{
    const user = new User({
      email: el.email,
      firstName: el.firstName,
      lastName: el.lastName,
      phone: el.phone,
    });
    user.save();
  });
  req.body.update.map(el => {
    User.update(
      { _id: el._id, updateDate: { $lte: el.updateDate } },
      {
        email: el.email,
        firstName: el.firstName,
        lastName: el.lastName,
        phone: el.phone,
        updateDate: el.updateDate
      },
      function(err, user) {
        console.log('Update User : ', user._id);
      }
    );
  });
  res.status(201).json("done");
});

router.put("/:id", function(req, res, next) {
  User.update(
    { _id: req.params.id },
    {
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      phone: req.body.phone,
      updateDate: new Date()
    },
    function(err) {
      if (err) {
        return next(err);
      }
      res.status(201).json("done");
    }
  );
});

export default router;
