import User from './User';

import express from 'express';

const router = express.Router();

router.use('/user', User);

export default router;
