import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { UserService } from "../../user.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.css"]
})
export class AddComponent implements OnInit {
  form = new FormGroup({
    firstName: new FormControl("", Validators.minLength(1)),
    lastName: new FormControl("", Validators.minLength(1)),
    email: new FormControl("", Validators.minLength(1)),
    phone: new FormControl("", Validators.minLength(1))
  });
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {}

  onSubmit(): void {
    if (navigator.onLine) {
      this.userService
        .addUser({
          ...this.form.value,
          updateDate: new Date()
        })
        .subscribe(() => this.router.navigate(["/"]));
    } else {
      UserService.addCachedUser({
        ...this.form.value,
        updateDate: new Date()
      });
      this.router.navigate(["/"])
    }
  }
}
