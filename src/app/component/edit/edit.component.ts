import { Component, OnInit } from "@angular/core";
import { UserService } from "./../../user.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl
} from "@angular/forms";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"]
})
export class EditComponent implements OnInit {
  user: any;
  form = new FormGroup({
    firstName: new FormControl("", Validators.minLength(1)),
    lastName: new FormControl("", Validators.minLength(1)),
    email: new FormControl("", Validators.minLength(1)),
    phone: new FormControl("", Validators.minLength(1))
  });
  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.user = this.userService.getUser(params["id"]).subscribe(data => {
        this.user = data;
        this.form = this.initForm(data);
      });
    });
  }

  onSubmit(): void {
    this.activatedRoute.params.subscribe(params => {
      if (navigator.onLine) {
        this.userService
          .updateUser(params["id"], {
            ...this.form.value,
            _id: this.user._id,
            updateDate: new Date()
          })
          .subscribe(() => this.router.navigate(["/"]));
      } else {
        UserService.addCachedUpdatedUser({
          ...this.form.value,
          _id: this.user._id,
          updateDate: new Date()
        });
        this.router.navigate(["/"]);
      }
    });
  }

  initForm(user) {
    return this.formBuilder.group({
      email: [user.email, Validators.required],
      firstName: [user.firstName, Validators.required],
      lastName: [user.lastName, Validators.required],
      phone: [user.phone, Validators.required]
    });
  }
}
