import { Component, OnInit, Input } from '@angular/core';
import {UserType} from './../../user'
@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input() user: UserType;
  @Input() pending: boolean;
  constructor() { }

  ngOnInit() {
  }

}
