import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { UserService } from "./../../user.service";
import { UserType, User } from "./../../user";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  users: UserType;
  usersCached: [UserType];
  updateCached: [UserType];
  constructor(private http: HttpClient, private userService: UserService) {}

  ngOnInit() {
    this.getUsers();
    this.sync();
  }

  pending = id => this.updateCached.find(el => el._id === id);

  getUsers() {
    this.userService.getUsers().subscribe((res: UserType) => {
      this.users = res;
    });
    this.usersCached = UserService.getCachedUsers();
    this.updateCached = UserService.getCachedUpdatedUser();
  }
  sync = () => {
    // i don't like using setTimeout here , instead i want to use WebSocket to broadcast an event from the server
    // to check if Client has something to sync
    setTimeout(() => {
      if (
        navigator.onLine &&
        (this.usersCached.length || this.updateCached.length)
      ) {
        this.userService.syncUser().subscribe(res => {
          this.getUsers();
        });
      }
      this.sync();
    }, 3000);
  };
}
