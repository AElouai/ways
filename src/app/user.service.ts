import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserType } from "./user";
import { map, tap } from "rxjs/operators";

const URL = "";
@Injectable({
  providedIn: "root"
})
export class UserService {
  result: any;
  constructor(private http: HttpClient) {}

  getUsers() {
    const uri = `${URL}/api/user`;
    return this.http.get(uri).pipe(
      map(res => res));
  }

  getUser(userId: string) {
    const uri = `${URL}/api/user/${userId}`;
    return this.http.get(uri).pipe(map(res => res));
  }

  updateUser(userId: string, user: UserType) {
    const uri = `${URL}/api/user/${userId}`;
    return this.http.put(uri, user);
  }

  addUser(user: UserType) {
    const uri = `${URL}/api/user`;
    return this.http.post(uri, user);
  }

  syncUser() {
    const uri = `${URL}/api/user/sync`;
    const usersUpdated = JSON.parse(localStorage.getItem("updated"));
    const users = JSON.parse(localStorage.getItem("users"));
    /*    this.http.post(uri, users).subscribe(
          data => {
            localStorage.setItem("users", JSON.stringify([]));
          },
          err => console.log("err : ", err)
        );*/
    return this.http
      .put(uri, {
        new: users,
        update: usersUpdated
      })
      .pipe(
        tap(val => {
          localStorage.setItem("updated", JSON.stringify([]));
          localStorage.setItem("users", JSON.stringify([]));
        })
      );

    /*    subscribe(
      data => {
        localStorage.setItem("updated", JSON.stringify([]));
        localStorage.setItem("users", JSON.stringify([]));
      });*/
  }

  static getCachedUsers() {
    const users = JSON.parse(localStorage.getItem("users"));
    return users || [];
  }
  static addCachedUser(user: UserType) {
    const users = JSON.parse(localStorage.getItem("users")) || [];
    users.push(user);
    localStorage.setItem("users", JSON.stringify(users));
  }
  static addCachedUpdatedUser(user: UserType) {
    const users = JSON.parse(localStorage.getItem("updated")) || [];
    users.push(user);
    localStorage.setItem("updated", JSON.stringify(users));
  }
  static getCachedUpdatedUser() {
    const users = JSON.parse(localStorage.getItem("updated"));
    return users || [];
  }
}
