export interface User {
  _id: string;
  lastName: string;
  firstName: string;
  email: string;
  phone: string;
}
export type UserType =  {
  _id: string;
  lastName: string;
  firstName: string;
  email: string;
  phone: string;
  updateDate: Date,
}
