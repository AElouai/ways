// routerConfig.ts

import { Routes } from '@angular/router';
import { AddComponent } from './component/add/add.component';
import { EditComponent } from './component/edit/edit.component';
import { UsersComponent } from './component/users/users.component';

export const appRoutes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  { path: 'add', component: AddComponent },
  { path: 'edit/:id', component: EditComponent},
  { path: 'users', component: UsersComponent }
];
